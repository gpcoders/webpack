import express from "express"

const server = express()

const webpack  = require("webpack")
const config = require('../../config/webpack.config')
const compiler = webpack(config)
require("webpack-mild-compile")(compiler)

const webpackDevMiddleware = require("webpack-dev-middleware")(
    compiler,
    config.devServer
)

const webpackHotMiddleware = require("webpack-hot-middleware")(compiler)

server.use(webpackDevMiddleware)
server.use(webpackHotMiddleware)

const staticMiddleware = express.static("dist")


server.use(staticMiddleware)

server.listen(8080, () => {
    console.log("server is listing")
})